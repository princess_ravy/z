module.exports = class Command {

	constructor(client) {
		this.client = client;
		this.name = 'test';
		this.aliases = ['t', 'lol'];
	}

	async run(msg, args) {
		return msg.channel.send('Test ' + args.join(' '));
	}

}