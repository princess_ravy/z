require('dotenv').config();

const { Client, MessageEmbed } = require('discord.js');
const client = new Client();

const moment = require('moment');

const { prefix, token } = require('./config.json');

client.on('ready', () => {
	console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', async msg => {
	if (!msg.content.startsWith(prefix) || msg.author.bot || !msg.guild) return;
	const app = await client.fetchApplication();
	client.owner = app.owner;

	const args = msg.content.slice(prefix.length).split(/ +/);
	const command = args.shift().toLowerCase();

	// util
	if (command === 'ping') {
		msg.channel.send('Ping?')
			.then(m => m
				.edit(`Pong! Took me ${(m.editedTimestamp || m.createdTimestamp) - (msg.editedTimestamp || msg.createdTimestamp)}ms.`));
	} 
	if (command === 'hello') {
		msg.channel.send(`Hi! My name is **${client.user.username}**!`);
	}
	if (command === 'die') {
		if (msg.author.id !== client.owner.id) return msg.reply('HEY that\'s RUDE');
		await msg.channel.send('Rude. Shutting down.');
		await client.destroy();
		process.exit(0);
	}

	// mod
	if (command === 'k' || command === 'kick') {
		if (!msg.mentions.members.size) return msg.reply('you need to tag a user in order to kick them!');
		const member = msg.mentions.members.first();
		const reason = args.slice(1).join(' ');
		if (member.kickable) {
			await msg.react('✅');
			return member.kick(reason);
		}
		return msg.reply('I cannot kick this user.');
	}
	if (command === 'b' || command === 'ban') {
		if (!msg.mentions.members.size) return msg.reply('you need to tag a user in order to ban them!');
		const member = msg.mentions.members.first();
		const reason = args.slice(1).join(' ');
		if (member.bannable) {
			await msg.react('✅');
			return member.ban({ reason });
		}
		return msg.reply('I cannot ban this user.');
	}

	// info
	if (command === 'ui' || command === 'user-info') {
	  	const member = msg.mentions.members.first() || msg.member;
	  	const embed = new MessageEmbed()
			.addField('Name', member.user.tag, true)
			.addField('ID', member.id, true)
			.addField('Discord joined', moment(member.user.createdAt).format('MMMM Do YYYY'), true)
			.addField('Joined at', moment(member.joinedAt).format('MMMM Do YYYY'), true)
			.setThumbnail(member.user.displayAvatarURL({ format: 'png', size: 1024 }))
			.setColor(msg.guild.me.displayColor);

			
	  	return msg.channel.send({embed});
	}

	if (command === 'ri' || command === 'role-info') {
	  	const role = msg.guild.roles.find(r => r.name.toLowerCase() === args.join(' ').toLowerCase());
	  	if (!role) return msg.channel.send('Invalid role.');

	  	const embed = new MessageEmbed()
	  		.addField('name', role.name, true)
	  		.addField('id', role.id, true)
	  		.addField('color', role.hexColor, true)
	  		.addField('created', moment(role.createdAt).format('MMMM Do YYYY'), true)
	  		.addField('hoisted', role.hoist ? 'Yes' : 'No', true)
	  		.addField('mentionable', role.mentionable ? 'Yes' : 'No', true)
	  		.setColor(msg.guild.me.displayColor);

	  	return msg.channel.send({embed});
	}

	if (command === 'si' || command === 'server-info') {
	  	const { guild } = msg;
	  	const embed = new MessageEmbed()
	  		.addField('name', guild.name, true)
	  		.addField('id', guild.id, true)
	  		.addField('created', moment(guild.createdAt).format('MMMM Do YYYY'), true)
	  		.addField('region', guild.region, true)
	  		.addField('owner', guild.owner.user.tag, true)
	  		.addField('members', guild.memberCount, true)
	  		.setThumbnail(guild.iconURL({ format: 'png', size: 1024 }))
	  		.setColor(msg.guild.me.displayColor);

	  	return msg.channel.send({embed});
	}

	if (command === 'clear' || command === 'prune') {
		const amount = parseInt(args[0]);
		if (amount === NaN || amount > 100 || amount < 1) return msg.reply('Invalid amount.');
		return msg.channel.bulkDelete(amount + 1, true).catch(() => null);
	}

	if (command === 'invite') {
		const embed = new MessageEmbed()
			.setDescription(`[Invite ${client.user.username}](https://discordapp.com/oauth2/authorize?client_id=${client.user.id}&scope=bot&permissions=93262)`);
		return msg.channel.send({ embed });
	}

});

client.login(/*token*/);