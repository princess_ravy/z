require('dotenv').config();
const 	fs = require('fs'),
		path = require('path');
const { Client, MessageEmbed, Collection } = require('discord.js');
const { prefix } = require('./config.json');

const client = new Client();
client.commands = new Collection();
client.aliases = new Collection();

client.on('ready', async () => {
	console.log(`Logged in as ${client.user.tag} (${client.user.id})!`);
	client.application = await client.fetchApplication();
});

commands = fs.readdirSync(path.join(process.cwd(), 'commands'));

for (const command of commands) {
	const Command = require(`./commands/${command}`);
	const cmd = new Command(client);
	for (const alias of cmd.aliases) {
		client.aliases.set(alias, cmd.name);
	}
	client.commands.set(cmd.name, cmd);
}

client.on('message', async (msg) => {
	if (!msg.content.startsWith(prefix) || msg.author.bot || !msg.guild) return;
	const args = msg.content.slice(prefix.length).split(/ +/);
	const command = args.shift().toLowerCase();
	if (client.commands.has(command)) return client.commands.get(command).run(msg, args);
	if (client.aliases.has(command)) return client.commands.get(client.aliases.get(command)).run(msg, args);
});

client.login();